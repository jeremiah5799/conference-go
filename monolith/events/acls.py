import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}"}
    response = requests.get(url, params=params, headers=headers)
    parsed_json = json.loads(response.content)
    picture = {"picture_url": parsed_json["photos"][0]["src"]["original"]}
    return picture


def get_lat_long(location):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city}, {location.state.abbreviation}, USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    parsed_json = json.loads(response.content)
    lat_lon = {
        "lat": parsed_json[0]["lat"],
        "lon": parsed_json[0]["lon"],
    }
    return lat_lon


def get_weather(location):
    lat_lon = get_lat_long(location)
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    response = requests.get(url, params=params)
    parsed_json = json.loads(response.content)
    weather = {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }
    return weather
